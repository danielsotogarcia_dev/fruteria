const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const frutaSchema = new Schema({
    name: String,
    qty: Number,
    color: [String],
    size: {
        h: Number,
        w: Number
    }
},{
  versionKey: false  
});

module.exports = mongoose.model("Fruta", frutaSchema, "frutas");