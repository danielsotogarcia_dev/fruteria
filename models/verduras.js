const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const verduraSchema = new Schema({
    name: String,
    qty: Number,
},{
  versionKey: false  
});

module.exports = mongoose.model("Verdura", verduraSchema, "verduras");