const { appInitConf } = require("./appConf");
const { mongoDbInit } = require("./dbConf");
const app = appInitConf();
mongoDbInit();

const Frutas = require("./models/frutas");
const Verduras = require("./models/verduras");

// GET filtrado
app.get("/api/frutas/filter", (req, res) => {

    Frutas.find(
        {$and:[{color:{$in:["rojo","verde"]}}, {"size.h":{$lte:15}}]},
        {name: 1, qty:1, _id:0},
        (err, data)=> {
        if(!err) {
            res.status(200).send({
                success: 'true',
                message: 'GET: Frutas',
                frutas: data,
            });
        }else {
            throw err;
        }
    });
    // .sort([["qty", 1]]);
    // .skip(2)
    // .limit(2)
});

// GET frutas
app.get("/api/frutas", (req, res) => {

    Frutas.find((err, data)=>{
        if(!err) {
            res.status(200).send({
                success: 'true',
                message: 'GET: Frutas',
                frutas: data,
            });
        }else {
            throw error;
        }
    });
});

// GET verduras
app.get("/api/verduras", (req, res) => {

    Verduras.find((err, data)=>{
        if(!err) {
            res.status(200).send({
                success: 'true',
                message: 'GET: Verduras',
                verduras: data,
            });
        }else {
            throw error;
        }
    });
});


// GET verduras
app.get("/api/todo", (req, res) => {
    Frutas.find((err, dataFrutas)=>{
        Verduras.find((err, dataVerduras)=>{
            if(!err) {
                res.status(200).send({
                    success: 'true',
                    message: 'GET: Verduras',
                    frutas: dataFrutas,
                    verduras: dataVerduras,
                });
            }else {
                throw error;
            }
        });
    });
});



// GET ONE
app.get("/api/frutas/:id", (req, res) => {
    Frutas.findById(req.params.id, (err, data) => {
        if(!err) {
            res.status(200).send({
                success: 'true',
                message: 'GET ONE: Frutas',
                fruta: data,
            });
        }else {
            throw error;
        }
    });
});

// POST ONE FRUTA
app.post("/api/frutas", (req, res) => {
    
    const fruta = new Frutas({
        name: req.body.name,
        qty: req.body.qty,
        color: req.body.color,
        size: req.body.size,
    });
    
    fruta.save((err) => {
        if(!err) {
            res.status(201).send({
                success: 'true',
                message: 'POST ONE: Fruta',
                fruta
            });
        }else {
            throw error;
        }
    });
});


// PUT modifica una fruta
app.put("/api/frutas/:id", (req, res) => {
    Frutas.findById(req.params.id, (err, data) => {

        if(err)
            throw err;

        data.name = req.body.name ? req.body.name : data.name;
        data.qty = req.body.qty ? req.body.qty : data.qty;
        
        data.color = req.body.color ? req.body.color : data.color 
        data.size = req.body.size ? req.body.size : data.size;
        
        data.save((err) => {
            if(!err) {
                res.status(201).send({
                    success: 'true',
                    message: 'PUT: Fruta',
                    fruta: data
                });
            }else {
                throw err;
            }
        });
    })
});


// DELETE modifica una fruta
app.delete("/api/frutas/:id", (req, res) => {
    Frutas.findByIdAndDelete(req.params.id, (err, data) => {

        if(err)
            throw err;
 
        res.status(200).send({
            success: 'true',
            message: 'DELETE: Fruta',
            fruta: data
        });
    })
});


/*
db.frutas.find( {$and:[{color:{$in:["rojo","verde"]}}, {"size.h":{$lte:5}}]})
{ "_id" : ObjectId("5e3541db47fdd82c2155fc1d"), "name" : "cereza", "qty" : 48, "color" : [ "rojo" ], "size" : { "h" : 5, "w" : 4 } }
{ "_id" : ObjectId("5e3541db47fdd82c2155fc1f"), "name" : "uva", "qty" : 178, "color" : [ "verde" ], "size" : { "h" : 4, "w" : 3 } }
*/



const PORT = 5000;
app.listen(PORT, function () {
    console.log(`API Fruteria corriendo en puerto ${PORT}`);
});