const mongoose = require('mongoose'); // libreria para conectar con mongodb

const mongoDbInit = () => {
    // Conexion con mongo
    mongoose.connect('mongodb://localhost/fruteria', { useNewUrlParser: true, useUnifiedTopology: true }, (err, res) => {
        if (err) {
            console.log('ERROR: connecting to Database. ' + err);
        } else {
            console.log('Connected to Database');
        }
    });
};


module.exports = {
    mongoDbInit: mongoDbInit,
}

